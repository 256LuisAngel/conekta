<?php

require_once("conekta/lib/Conekta.php");

class MyConekta {
	
	public static $api_key = 'key_rYSifWptKB4TrPNxFCrMTQ';
	public static $description = 'Donation';	
	public static $currency = 'mxn';	

	//Function to validate if the token does exist
	public static function check_token($token){
		if ($token == $_SESSION['token'])
			return true;

		return false;
	}

	//Function to generate a md5 32digits token
	public static function tokengenerator($len = 32){		
		//seed
		$keychars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";				
		// RANDOM KEY GENERATOR
		$id = "";
		$max = strlen($keychars) - 1;
		
		for ($i=0;$i<$len;$i++)
			$id .= substr($keychars, rand(0, $max), 1);
		
		return md5($id);
	}

	//Function to make payments with a Credit/Debit Card
	public static function card($amount, $number, $exp_month, $exp_year, $cvc, $name){

		Conekta::setApiKey(self::$api_key);

		$data = array(
			'card' => array(
				'number' => $number, 
				'exp_month' => $exp_month, 
				'exp_year' => $exp_year, 
				'cvc' => $cvc, 
				'name' => $name
				), 
			'description' => self::$description, 
			'amount' => $amount, 
			'currency' => self::$currency
			);

		try {
		  $charge = Conekta_Charge::create($data);  
		  echo 'Thanks for your donation';
		} 
		catch (Exception $e) {
		  // Catch all exceptions including validation errors.
		  echo $e->getMessage(); 
		}
	}
	//END OF CLASS
}